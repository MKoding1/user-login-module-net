using System;
using src.Application;

namespace src.Infrastructure
{
    public class FacebookSessionManager : SessionManager
    {
        public bool Login(string userName, string password)
        {
            //Imaginad que esto en realidad realiza una llamada al API de Facebook
            return new Random().Next(1) == 1;
        }
        
        public int GetSessions()
        {
            //Imaginad que esto en realidad realiza una llamada al API de Facebook
            return new Random().Next(100);
        }
        
        public void Logout(string userName)
        {
            //Aquí llamaríamos a Facebook
        }

        public void SecureLogin(string userName)
        {
            //Aquí llamaríamos a Facebook
        }
    }
}