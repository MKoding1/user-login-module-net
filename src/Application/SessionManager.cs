namespace src.Application
{
    public interface SessionManager
    {
        int GetSessions();

        bool Login(string userName, string password);
        
        void Logout(string userName);
        
        void SecureLogin(string userName);
    }
}