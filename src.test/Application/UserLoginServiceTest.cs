﻿using NUnit.Framework;
using src.Application;

namespace src.test.Application
{
    [TestFixture]
    public class UserLoginServiceTest
    {
        [Test]
        public void UserIsLoggedIn()
        {
            var userLoginService = new UserLoginService();
            
            var result = userLoginService.ManualLogin();
            
            Assert.That(result, Is.EqualTo("user logged"));
        }
    }
}