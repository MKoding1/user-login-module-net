# NET Framework user login module Kata
Esta es una kata con .NET 4.8 y NUnit para trabajar con diferentes dobles de tests :)

## Uso
- Utilizar el IDE Visual Studio 2019 (Windows) o Jetbrains Rider (MacOS, Linux, Windows).

## Requisitos
- .NET Framework 4.8
- NUnit 3.12.0